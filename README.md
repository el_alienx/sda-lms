# SDA Front End

## Install instructions

### Pre-requisites

1. Downnload and install the latest Long Term Support (LTS) version of Node.js
1. Install Typescript npm install typescript
1. Install Firebase Tools sudo npm install -g firebase-tool
1. Run firebase login using the softwaredevacademy@gmail.com account (ask your supervisor for the password)

### Normal runtime

- npm start
