// Core
import React, { useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useRecoilState } from "recoil";

// Internal
import SwitchLogged from "./pages/switch-logged/SwitchLogged";
import SwitchUnlogged from "./pages/switch-unlogged/SwitchUnlogged";
import { userIdState } from "state/userState";
import "./styles/style.sass";

export default function App() {
  // State
  const [userId, setUserId] = useRecoilState(userIdState);

  // Methods
  useEffect(() => {
    document.title = "SDA Platform";

    const userId = localStorage.getItem("userId");
    if (userId) setUserId(userId);
  }, [setUserId]);

  return (
    <Router>
      <div className="App">
        {userId ? <SwitchLogged /> : <SwitchUnlogged />}
      </div>
    </Router>
  );
}
