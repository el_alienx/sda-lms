import { eCourseTpye } from "./eCourseType";

export default interface iCourse {
  date: string;
  description: string;
  id: string;
  image: string;
  name: string;
  priority: number;
  type: eCourseTpye;
}
