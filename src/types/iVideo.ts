export default interface iVideo {
  id: number;
  name: string;
  youtubeCode: string;
  duration: string;
}
