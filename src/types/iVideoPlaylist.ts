import iVideo from "./iVideo";

export default interface iVideoPlayList {
  id: string;
  courseId: string;
  name: string;
  description: string;
  additionalResources: string;
  list: Array<iVideo>;
}
