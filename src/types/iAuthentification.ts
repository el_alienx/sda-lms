export default interface iAuthentification {
  name: string;
  email: string;
  password: string;
  code: string;
}
