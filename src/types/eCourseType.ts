export enum eCourseTpye {
  document = "document",
  slide = "slide",
  video = "video",
  workshop = "workshop",
  coding = "coding",
  evaluation = "evaluation",
}
