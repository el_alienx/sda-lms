// Credit: https://www.digitalocean.com/community/tutorials/how-to-use-font-awesome-5-with-react
// Clean up imports by using: https://sortmylist.com

/* 
 About:
 This file takes care of importing the select icons of the Font Awesome 5 library.
 So far is the best method that I know that does not includes everysingle icon whatherver we use or not.
 If somebody finds a better method, please share to make the respective change. - Eduardo Alvarez.
*/

// Core
import { library } from "@fortawesome/fontawesome-svg-core";

// Icons
import {
  faBookOpen,
  faCalendarAlt,
  faChalkboardTeacher,
  faCode,
  faCommentAlt,
  faExclamationCircle,
  faFileAlt,
  faFileCode,
  faFolder,
  faImage,
  faLayerGroup,
  faListUl,
  faPencilAlt,
  faPlay,
  faUser,
  faInfo,
  faGlobe,
  faProjectDiagram,
  faLaptopCode,
  faTrophy,
  faLock,
  faUsers,
  faExternalLinkAlt,
  faPlayCircle,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faBookOpen,
  faCalendarAlt,
  faChalkboardTeacher,
  faCode,
  faCommentAlt,
  faExclamationCircle,
  faFileAlt,
  faFileCode,
  faFolder,
  faImage,
  faLayerGroup,
  faListUl,
  faPencilAlt,
  faPlay,
  faUser,
  faInfo,
  faGlobe,
  faProjectDiagram,
  faLaptopCode,
  faTrophy,
  faLock,
  faUsers,
  faExternalLinkAlt,
  faPlayCircle
);
