import { atom } from "recoil";

/*
About;
This file handles the user profile.
Each time the user logins, the atom is populated.
This if the App.jsx should render the login or logout pages.
*/

// Atoms
/* ID of the currently selected course */
export const userIdState = atom({
  key: "id",
  default: null,
});

export const userState = atom({
  key: "user",
  default: {},
});
