// Core
import React from "react";
import { NavLink, Link } from "react-router-dom";
import { useRecoilState } from "recoil";

// Internal
import ItemNavigation from "components/molecules/ItemNavigation";
import Anchor from "components/atoms/Anchor";
import Logo from "assets/images/logo.svg";
import Avatar from "components/atoms/Avatar";
import { userState } from "state/userState";

export default function NavigationBar() {
  // State
  const [user] = useRecoilState(userState);

  // Properties
  const calendarURL =
    "https://calendar.google.com/calendar/r?cid=ojq08n0ik1ag8jc394nij0ccfk@group.calendar.google.com";
  const chatURL = "https://app.slack.com/client/TT8QR5H1D/CSU5LMSJE";
  const { profile_picture } = user;

  return (
    <nav className="navigation-bar">
      {/* Logo */}
      <Link to="/" className="logo">
        <img src={Logo} alt="An circle with the letters S, D, and A inside" />
      </Link>

      <hr />

      {/* Options */}
      <NavLink
        className="item-navigation"
        activeClassName="selected"
        exact
        to="/"
      >
        <ItemNavigation icon="book-open" label="Courses" />
      </NavLink>
      <Anchor className="item-navigation" href={calendarURL}>
        <ItemNavigation icon="calendar-alt" label="Calendar" />
      </Anchor>
      <Anchor className="item-navigation" href={chatURL}>
        <ItemNavigation icon="comment-alt" label="Chat" />
      </Anchor>

      {/* User avatar */}
      <NavLink
        className="item-navigation profile"
        activeClassName="selected"
        to="/profile"
      >
        <ItemNavigation icon="user" label="Profile" />
        <Avatar picture={profile_picture} />
      </NavLink>
    </nav>
  );
}
