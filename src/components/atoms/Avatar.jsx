// Core
import React from "react";

// Media
import TempAvatar from "assets/images/avatar-default.png";

export default function Avatar({ picture }) {
  const image = picture !== undefined ? picture : TempAvatar;

  return <img className="avatar" src={image} alt="Your profile avatar" />;
}
