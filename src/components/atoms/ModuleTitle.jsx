import React from "react";

export default function ModuleTitle({ id, name }) {
  return <h3 className={`course color-${id}`}>{name}</h3>;
}
