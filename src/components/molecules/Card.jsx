// Core
import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/fontawesome";

export default function Card({ props, route }) {
  const { image, date, name } = props;

  const Padlock = (
    <div className="lock">
      <div className="icon">
        <FontAwesomeIcon icon="lock" />
      </div>
      <div>
        Starts on <br />
        {date}
      </div>
    </div>
  );

  const Item = (
    <React.Fragment>
      <div className="image-container">
        {date ? Padlock : null}
        <img className="image" src={image} alt={`${name} module cover`} />
      </div>
      <div className="label">{name}</div>
    </React.Fragment>
  );

  const ActiveItem = (
    <Link className="card" to={route}>
      {Item}
    </Link>
  );
  const InactiveItem = <div className="card">{Item}</div>;

  return date ? InactiveItem : ActiveItem;
}
