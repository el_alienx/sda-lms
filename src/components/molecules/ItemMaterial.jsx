// Core
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/fontawesome";

export default function ItemMaterial({ color, prop }) {
  let { date, icon = "file-alt", name } = prop;

  if (date) {
    color = "lock";
    icon = "lock";
  }

  const OpenDate = (
    <React.Fragment>
      <br />
      <small>(opens on {date})</small>
    </React.Fragment>
  );

  // The content of the component that always stay the same
  return (
    <React.Fragment>
      <span className={`icon color-${color}`}>
        <FontAwesomeIcon icon={icon} />
      </span>
      <span className="label">
        {name}
        {date ? OpenDate : null}
      </span>
    </React.Fragment>
  );
}
