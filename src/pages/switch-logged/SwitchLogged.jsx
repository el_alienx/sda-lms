// Core
import React, { useEffect, useCallback } from "react";
import { useRecoilState } from "recoil";
import { Switch, Route } from "react-router-dom";

// Internal
import { coursesState } from "state/courseState";
import { userState, userIdState } from "state/userState";
import Model from "./Model";

// Pages
import Course from "pages/course/Course";
import Dashboard from "pages/dashboard/Dashboard";
import Login from "pages/login/Login";
import Page404 from "pages/page404/Page404";
import Profile from "pages/profile/Profile";
import RecoverPassword from "pages/recover-password/RecoverPassword";
import SignUp from "pages/sign-up/SignUp";
import VideoPlaylist from "pages/video-playlist/VideoPlaylist";

export default function SwitchLogged() {
  // State
  const [, setCourses] = useRecoilState(coursesState);
  const [userId] = useRecoilState(userIdState);
  const [, setUser] = useRecoilState(userState);

  // Methods
  const fetchCourses = useCallback(
    async (model) => {
      const rawCourses = await model.getCourses();
      const sortedCourses = rawCourses.sort((a, b) => a.priority - b.priority);

      setCourses(sortedCourses);
    },
    [setCourses]
  );

  const fetchUser = useCallback(
    async (model) => {
      const user = await model.getUser(userId);

      setUser(user);
    },
    [userId, setUser]
  );

  useEffect(() => {
    const model = new Model();

    fetchCourses(model);
    fetchUser(model);
  }, [fetchCourses, fetchUser]);

  return (
    <Switch>
      <Route path="/" exact component={Dashboard} />
      <Route path="/course/:id" component={Course} />
      <Route path="/profile" component={Profile} />
      <Route path="/sign-up" component={SignUp} />
      <Route path="/login" component={Login} />
      <Route path="/recover-password" component={RecoverPassword} />
      <Route path="/video-playlist/:videoId" component={VideoPlaylist} />
      <Route component={Page404} />
    </Switch>
  );
}
