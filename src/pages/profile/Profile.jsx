// Core
import React, { useEffect } from "react";
import { useRecoilState } from "recoil";

// Internal
import Avatar from "components/atoms/Avatar";
import Page from "components/templates/Page";
import { userState, userIdState } from "state/userState";
import "./Style.sass";

export default function Profile() {
  // State
  const [user, setUser] = useRecoilState(userState);
  const [, setUserId] = useRecoilState(userIdState);

  // Properties
  const { name = "My name", iteration = "SDA 9", profile_picture } = user;

  // Methods
  useEffect(() => {
    document.title = "SDA Student profile";
  });

  function logout() {
    localStorage.clear();
    setUser(null);
    setUserId(null);
  }

  return (
    <Page id="profile">
      <h1>
        Student <br />
        Profile
      </h1>

      <h2>Personal information</h2>
      <Avatar picture={profile_picture} />
      <h3>{name}</h3>
      <span className="label">{iteration}</span>

      <footer className="footer">
        <hr />
        <button className="button" onClick={logout}>
          Logout
        </button>
      </footer>
    </Page>
  );
}
